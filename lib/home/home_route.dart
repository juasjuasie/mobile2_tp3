import 'package:flutter/material.dart';
import 'package:tp3/app/durations.dart';
import 'package:tp3/app/strings.dart';
import 'package:tp3/train_page/train_on_language_page.dart';
import '../learn_page/learn_lang_page.dart';

class HomeRoute extends StatefulWidget {
  static const _pageLearnStorageKey = "learnKey";
  static const _pageTrainStorageKey = "trainKey";

  const HomeRoute({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomeRouteState();
  }
}

class HomeRouteState extends State<HomeRoute> {
  final _controller = PageController();
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    Strings strings = Strings.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(strings.title),
      ),
      body: PageView(
        controller: _controller,
        children: [
          LearnLanguagePage(
            key: PageStorageKey(HomeRoute._pageLearnStorageKey),
          ),
          TrainOnLanguagePage(
            key: PageStorageKey(HomeRoute._pageTrainStorageKey),
          ),
        ],
        onPageChanged: _onPageChanged,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            title: Text(strings.learn),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.create),
            title: Text(strings.train),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blueAccent,
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int actualIndex) {
    setState(() {
      _changeIndex(actualIndex);
    });
    _controller.animateToPage(actualIndex, duration: Durations.pageTransitionDuration, curve: Curves.fastOutSlowIn);
  }

  void _onPageChanged(int actualIndex) {
    setState(() {
      _changeIndex(actualIndex);
    });
  }

  void _changeIndex(int actualIndex) {
    if (_selectedIndex != actualIndex) {
      _selectedIndex = actualIndex;
    }
  }
}
