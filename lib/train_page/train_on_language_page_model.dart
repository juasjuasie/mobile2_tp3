import 'dart:math';
import 'package:tp3/grapheme/grapheme_model.dart';

class TrainOnLanguagePageModel {
  final int _numberOfChoices;
  final List<GraphemeModel> _graphemes;

  List<GraphemeModel> _possibleAnswers;
  int _chosenGrapheme;
  List<bool> _isWrongAnswerStatus;

  List<GraphemeModel> get possibleAnswers => _possibleAnswers;
  List<bool> get isWrongAnswerList => _isWrongAnswerStatus;
  int get chosenGrapheme => _chosenGrapheme;

  TrainOnLanguagePageModel(this._graphemes, this._numberOfChoices) {
    _isWrongAnswerStatus = List.filled(_numberOfChoices, false);
    _shuffleChoices();
  }

  void _shuffleChoices() {
    final rng = Random();
    var possibleAnswerList = Set<GraphemeModel>();

    while (possibleAnswerList.length != _numberOfChoices) {
      possibleAnswerList.add(_graphemes[rng.nextInt(_graphemes.length - 1)]);
    }
    _possibleAnswers = possibleAnswerList.toList();
    _possibleAnswers.shuffle();
    _chosenGrapheme = rng.nextInt(_possibleAnswers.length - 1);
  }

  void verifyAnswerIsCorrect(int choice) {
    if (choice == _chosenGrapheme) {
      _restartTraining();
    } else {
      _changeStatusToWrong(choice);
    }
  }

  void _restartTraining() {
    _isWrongAnswerStatus = List.filled(_numberOfChoices, false);
    _shuffleChoices();
  }

  void _changeStatusToWrong(int index) {
    _isWrongAnswerStatus[index] = true;
  }
}
