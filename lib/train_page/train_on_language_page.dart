import 'package:flutter/material.dart';
import 'package:tp3/app/config.dart';
import 'package:tp3/grapheme/grapheme_card.dart';
import 'package:tp3/train_page/train_on_language_page_model.dart';

class TrainOnLanguagePage extends StatefulWidget {
  const TrainOnLanguagePage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return TrainOnLanguagePageState();
  }
}

class TrainOnLanguagePageState extends State<TrainOnLanguagePage>
    with AutomaticKeepAliveClientMixin<TrainOnLanguagePage> {
  final _trainModel = new TrainOnLanguagePageModel(Config.graphemes, Config.trainNbChoices);

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final _columnWidgets = List<Widget>();

    _columnWidgets.add(
      Expanded(
        child: GraphemeCard(
          grapheme: _trainModel.possibleAnswers[_trainModel.chosenGrapheme],
          showsTranslation: false,
        ),
      ),
    );
    _columnWidgets.addAll(_buildButtons());

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: _columnWidgets,
    );
  }

  List<Widget> _buildButtons() {
    var buttons = List<OutlineButton>();
    for (int i = 0; i < Config.trainNbChoices; i++) {
      buttons.add(
        OutlineButton(
          disabledTextColor: Colors.redAccent,
          child: Text(
            _trainModel.possibleAnswers[i].translation,
          ),
          onPressed: _trainModel.isWrongAnswerList[i]
              ? null
              : () => _onChoiceButtonPressed(
                    context,
                    i,
                  ),
        ),
      );
    }
    return buttons;
  }

  void _onChoiceButtonPressed(BuildContext context, int buttonIndex) {
    setState(() {
      _trainModel.verifyAnswerIsCorrect(buttonIndex);
    });
  }
}
