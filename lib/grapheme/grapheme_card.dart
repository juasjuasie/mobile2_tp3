import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tp3/grapheme/grapheme_model.dart';

class GraphemeCard extends StatelessWidget {
  static const double padding = 16.0;

  final GraphemeModel _grapheme;
  final bool _showsTranslation;

  const GraphemeCard({Key key, @required GraphemeModel grapheme, @required bool showsTranslation})
      : _grapheme = grapheme,
        _showsTranslation = showsTranslation,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(padding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: FittedBox(
                child: Text(
                  _grapheme.symbol,
                ),
              ),
            ),
            if (_showsTranslation)
              Text(
                _grapheme.translation,
                style: Theme.of(context).textTheme.body1,
              ),
          ],
        ),
      ),
    );
  }
}
