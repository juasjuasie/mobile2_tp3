class GraphemeModel {
  final symbol;
  final translation;

  const GraphemeModel({String symbol, String translation})
      : symbol = symbol,
        translation = translation;
}
