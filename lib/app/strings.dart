import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class Strings {
  static const values = {
    "en": {
      "title": "Hiraganas",
      "bottom_nav_learn": "Learn",
      "bottom_nav_train": "Train",
    },
    "fr": {
      "title": "Hiraganas",
      "bottom_nav_learn": "Apprendre",
      "bottom_nav_train": "Entrainement",
    }
  };

  final Map<String, String> _values;

  String get title => _values["title"];
  String get learn => _values["bottom_nav_learn"];
  String get train => _values["bottom_nav_train"];

  Strings(Locale locale) : _values = values[locale.languageCode];

  Strings.of(BuildContext context) : this(Localizations.localeOf(context));
}

class AppLocalizationsDelegate extends LocalizationsDelegate<Strings> {
  const AppLocalizationsDelegate._internal();

  static const AppLocalizationsDelegate delegate = AppLocalizationsDelegate._internal();

  @override
  bool isSupported(Locale locale) => Strings.values.containsKey(locale.languageCode);

  @override
  Future<Strings> load(Locale locale) => SynchronousFuture<Strings>(Strings(locale));

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}
