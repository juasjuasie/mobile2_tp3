import 'package:flutter/material.dart';
import 'package:tp3/app/config.dart';
import 'package:tp3/grapheme/grapheme_card.dart';

class LearnLanguagePage extends StatelessWidget {
  static const _crossAxisCount = 2;

  const LearnLanguagePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: _crossAxisCount),
        itemCount: Config.graphemes.length,
        itemBuilder: (_, i) {
          return GraphemeCard(
            grapheme: Config.graphemes[i],
            showsTranslation: true,
          );
        },
      ),
    );
  }
}
